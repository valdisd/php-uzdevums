@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach ($newsArticles as $article)
                <div class="card">
                    <img class="card-img-top" src="{{$article->imageUrl}}" alt="article-image" />
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{ $article->url }}">{{ $article->title }}</a></h5>
                        <p class="card-text">{{ $article->description }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
