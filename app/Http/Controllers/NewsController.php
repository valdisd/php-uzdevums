<?php

namespace App\Http\Controllers;

use App\Services\NewsService;
use Illuminate\Support\Facades\View;

class NewsController extends Controller
{
    /** @var NewsService */
    private $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->middleware('verified');
        $this->newsService = $newsService;
    }

    public function index()
    {
        $newsArticles = $this->newsService->getNews();

        return View::make('news.index', compact('newsArticles'));
    }
}
