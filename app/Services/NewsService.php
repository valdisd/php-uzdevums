<?php

namespace App\Services;
use App\Structures\NewsArticle;

class NewsService
{
    const TVNET_RSS_FEED_URL = "https://www.tvnet.lv/rss";
    // Note: If you need to use multiple feeds in the future, create a feed interface and abstract class

    const ARTICLE_COUNT = 5;

    /**
     * @return NewsArticle[]
     */
    public function getNews()
    {
        /*
         * Note: If RSS feed is unstable implement additional permanent caching or store in DB
         * or check SimplePie::force_cache_fallback()
         */
        return $this->getNewsFromFeed();
    }

    /**
     * Get NewsArticle array from RSS feed
     *
     * @return NewsArticle[]
     */
    private function getNewsFromFeed()
    {
        $feed = \Feeds::make(self::TVNET_RSS_FEED_URL);

        $items = $feed->get_items();

        $newsArray = [];
        $counter = 0;
        foreach ($items as $item) {
            $enclosure = $item->get_enclosure();

            $article = new NewsArticle();
            $article->title = $item->get_title();
            $article->description = $item->get_description();
            $article->url =  $item->get_permalink();
            $article->date = $item->get_date();
            $article->imageUrl = $enclosure->get_link();

            $newsArray[] = $article;
            $counter++;
            if ($counter == self::ARTICLE_COUNT) break;
        }

        return $newsArray;
    }
}