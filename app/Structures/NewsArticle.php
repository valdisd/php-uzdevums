<?php

namespace App\Structures;

class NewsArticle
{
    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var string */
    public $date;

    /** @var string */
    public $url;

    /** @var string */
    public $imageUrl;
}